# LIGO Channel Activity Monitor (ligocam).

`ligocam` is an efficient diagnostic tool for monitoring auxiliary channels.
The utilities include locating a malfunctioning channel, graphic information
of channel's time series and spectral data, and spectral change to understand
various band-limited environmental disturbances of non-astrophysical origin.

More information can be found here: http://pem.ligo.org

## Conda environment setup and installation
This ensures dependencies are set up properly. Clone the repository:
```
git clone https://git.ligo.org/pem/ligocam.git
```
Create a conda environment from the config file:
```
cd ligocam
conda env create -f environment.yaml
```
Activate the conda environment and install `ligocam`:
```
conda activate ligocam
pip install .
```

## Setup
Each ifo and subsystem is run from its own config file. For first-time setup,
edit the configuration files located in `/etc/config/` for the desired ifo
and subsystem to your liking and feed it to `ligocam-setup` with the directory
containing the necessary static files, e.g.
```
ligocam-setup <config_file> <share_directory>
```
This will create a directory structure where outputs will be saved.

When you're ready to get `ligocam` running, your run directory should be
structured like this:
```
/
- config
    - <channel lists>
    - <config .ini files>
    - <thresholds.ini>
- LHO/LLO
    - <subsystem folders, e.g. PEM>
        - history
        - jobs
```
Your output directory should look like this:
```
- LHO/LLO
    - <subsystem folders, e.g. PEM>
        - calendars
        - css
        - images
        - pages
        - results
        - status
```

## Running ligocam
Submit a batch of ligocam jobs to condor by running ligocam-batch with
the desired config file:
```
ligocam-batch <config_file>
```

## Resetting a channel's history
Acceptable reference PSDs and the number of hours a channel has been
disconnected or had a DAQ failure are all logged in the run directory.
These records can be deleted, restarting the channel's alert counter, with
```
ligocam-reset <config_file> <channel_name>
```
