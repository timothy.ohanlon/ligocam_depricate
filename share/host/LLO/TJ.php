<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>LigoCAM @ LLO | PEM main</title>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    
    <body align="center">
        <div class="header" style="color:white; background-color:#7FA009;">
                  <h1>LigoCAM @ LLO | PEM </h1>
        </div>

        <div class="boxedmid">
            <form action="https://ldas-jobs.ligo-la.caltech.edu/~timothy.ohanlon/ligocam/PEM/LigoCamHTML_current.html" target="_blank">
                <p style="text-align: center;"><input type="submit" value="Latest page" style="width:60%; height:40px; color:#87099D; background-color:#D9BDDF; font-size:20px; margin-top:0px;"/></p>
            </form>
        </div> 
        
        <div class="container">
            <?php
                $textcolors = array("0A67A1", "FF9900", "298000", "7D3C98");
                $cellcolors = array("D8DCDE", "FFFFCC", "caffb3", "E8DAEF");
                $months = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");
                $years = array();
                $yearmonths = array();
                $calendars = glob("../PEM/calendar/*.html");
                foreach($calendars as $calendar) {
                    preg_match_all('!\d+_\d+!', basename($calendar), $matches);
                    $yearmonth = $matches[0][0];
                    $year = explode("_", $yearmonth)[0];
                    array_push($yearmonths, $yearmonth);
                    if (!in_array($year, $years)) {
                        array_push($years, $year);
                    }
                }
                foreach($years as $year) {
                    echo "<div class=\"boxedcalendarcenter\">\n";
                    foreach($months as $num => $month) {
                        $num_padded = sprintf("%02d", $num);
                        $datestring = $year . "_" . $num_padded;   // Formats the date to look like e.g. 01_2014 for Jan 2014
                        $url = "https://ldas-jobs.ligo-la.caltech.edu/~timothy.ohanlon/ligocam/PEM/calendar/LigoCAM_" . $datestring . ".html";
                        $color = $textcolors[$year % count($textcolors)];
                        $bgcolor = $cellcolors[$year % count($cellcolors)];
                        echo "\t\t\t\t";
                        echo "<form action=\"" . $url . "\" target=\"_blank\">\n";
                        echo "\t\t\t\t\t";
                        echo "<p style=\"text-align: center;\"><input type=\"submit\" value=\"" . $month . " " . $year . "\" ";
                        echo "style=\"width:100%; height:40px; color:#" . $color . "; background-color:#" . $bgcolor . "; font-size:20px; margin-top:0px;\"/>\n";
                        echo "\t\t\t\t";
                        echo "</form>\n";
                    }
                    echo "\t\t\t</div>\n\t\t\t";
                }
            ?>
        </div>
        <br>
        <div id="footer" style="background-color:white; float:bottom;">
            <table width="100%" height="3%"><tbody><tr><td align="right">Contact: dipongkar.talukder@ligo.org &nbsp;</td></tr></tbody></table>
        </div>
    </body>
</html>
